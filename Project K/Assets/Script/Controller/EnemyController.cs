using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public Animator animator;
    public float lookRadius;
    public float attackRadius;
    Transform target;
    public GameObject Blockdistance;
    public bool right;
    public bool hitWall;
    public PlayerGroundDetect playerGroundDetect;
    //public NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        playerGroundDetect= GameObject.FindGameObjectWithTag("PlayerGroundDetect").GetComponent<PlayerGroundDetect>();
        animator = GetComponentInParent(typeof(Animator)) as Animator;
        target = PlayerManager.Instance.player.transform;
        animator.SetBool("Patrol", true);
        //agent=GetComponentInParent(typeof(NavMeshAgent)) as NavMeshAgent;

    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);
       
        Debug.DrawRay(Blockdistance.transform.position, this.transform.forward * 0.3f, Color.red);
        RaycastHit hit;
        if (Physics.Raycast(Blockdistance.transform.position, this.transform.forward, out hit, 0.3f))
        {
            if (hit.collider.CompareTag("Wall"))
            {
                hitWall = true;
                //animator.SetBool("HitWall", true);


            }
  
        }
        
       
        
        if (distance <= lookRadius)
        {
            
            if (!hitWall&& playerGroundDetect.LowGround)
            {
                FaceDirection();
                animator.SetBool("IsFollowing", true);
                animator.SetBool("Out", false);
                animator.SetBool("Patrol", false);
                if (distance <= attackRadius)
                {
                    animator.SetBool("IsFollowing", false);
                    animator.SetBool("Patrol", false);
                    animator.SetBool("Attack", true);
                    

                }
                else
                {
                    animator.SetBool("Attack", false);
                }
            }
            if(hitWall&&playerGroundDetect.HightGround)
            {
                //FaceDirection();
                float backDistance = Vector3.Distance(target.position, transform.position);
                if (animator.GetBool("Patrol") )
                {
                    //Debug.Log("back");
                    
                    
                        hitWall = false;
                        //animator.SetBool(("HitWall"), false);
                    
                        
                }
                animator.SetBool("IsFollowing", false);
                animator.SetBool("Patrol", true);
               
                
                
            }
            
        }
        else
        {
            animator.SetBool("IsFollowing", false);
            animator.SetBool("Out", true);
            hitWall = false;
            //animator.SetBool(("HitWall"), false);
        }
    }
    void FaceDirection()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }
}
