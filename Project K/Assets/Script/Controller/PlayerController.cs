using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    
    public Animator animator;
    public Rigidbody rigid;
    public GameObject colliderEdgePrefab;
    public List<GameObject> BottomSphere=new List<GameObject>();
    public List<GameObject> FrontSphere = new List<GameObject>();

    public float gravityMultiplier;
    public float pullMultiplier;
    private  Vector3 Zfixed=new Vector3(0f,0f,0f);

    public bool IsStun;
    private float StunTime;
    public float CoolDownStun;
    private KeyboardInput _kbInput;
    public MoveForword _moveForword;

    private void Awake()
    {
        BoxCollider box = GetComponent<BoxCollider>();

        float bottom = box.bounds.center.y - box.bounds.extents.y;
        float top = box.bounds.center.y + box.bounds.extents.y;
        float front= box.bounds.center.x + box.bounds.extents.x;
        float back= box.bounds.center.x - box.bounds.extents.x;

        GameObject bottomFront = CreateEdgeSphere(new Vector3(front, bottom, -0.23f));
        GameObject bottomBack= CreateEdgeSphere(new Vector3(back, bottom, -0.23f));
        GameObject topFront = CreateEdgeSphere(new Vector3(front+0.02f, top, -0.23f));
        
        bottomFront.transform.parent = this.transform;
        bottomBack.transform.parent = this.transform;
        topFront.transform.parent = this.transform;

        BottomSphere.Add(bottomFront);
        BottomSphere.Add(bottomBack);


        FrontSphere.Add(bottomFront);
        FrontSphere.Add(topFront);

        float horSec = (bottomFront.transform.position - bottomBack.transform.position).magnitude / 5f;
        CreateMiddleSpheres(bottomFront, -this.transform.forward, horSec, 4, BottomSphere);

        float verSec = (bottomFront.transform.position - topFront.transform.position).magnitude / 10f;
        CreateMiddleSpheres(bottomFront, this.transform.up, verSec, 9, FrontSphere);
    }
    private void FixedUpdate()
    {
        
        Zfixed = this.transform.position;
        Zfixed.z = -0.23f;
        this.transform.position =Zfixed ;
        if (rigid.velocity.y < 0f)
        {
            rigid.velocity += (-Vector3.up * gravityMultiplier);

        }
        if(rigid.velocity.y > 0f&& !VirtualInputManager.Instance.Jump)
        {
            rigid.velocity += (-Vector3.up * pullMultiplier);
        }
    }
    public void CreateMiddleSpheres(GameObject start,Vector3 dir,float sec,int interation,List<GameObject> sphereLish)
    {
        for (int i = 0; i < interation; i++)
        {
            Vector3 pos = start.transform.position + (dir * sec * (i + 1));

            GameObject newObj = CreateEdgeSphere(pos);
            newObj.transform.parent = this.transform;
            sphereLish.Add(newObj);
        }
    }
    public GameObject CreateEdgeSphere(Vector3 pos)
    {
        GameObject obj = Instantiate(colliderEdgePrefab, pos, Quaternion.identity);
        return obj;
    }

    private void Update()
    {
        _kbInput = FindObjectOfType<KeyboardInput>();
        if (IsStun)
        {
            StunCoolDown();
        }
        //else
        //    _kbInput.gameObject.GetComponent<KeyboardInput>().stuned = false;
    }
    public void StunCoolDown()
    {
        Debug.Log("Hit");
        _kbInput.gameObject.GetComponent<KeyboardInput>().stuned = true;
        animator.SetBool("Move", false);
        _moveForword.Speed = 0;
        if (Time.time - StunTime < CoolDownStun)
        {
            return;
        }
        StunTime = Time.time;
        IsStun = false;
        _kbInput.gameObject.GetComponent<KeyboardInput>().stuned = false;
        _moveForword.Speed = 4;
        Debug.Log("DeStun");
    }
}
