using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : MonoBehaviour
{
    public bool stuned;
    // Update is called once per frame
    void Update()
    {
        if (stuned)
            return;
        if (Input.GetKey(KeyCode.D))
        {
            VirtualInputManager.Instance.MoveRight = true;
        }
        else
        {
            VirtualInputManager.Instance.MoveRight = false;
        }
        if (Input.GetKey(KeyCode.A))
        {
            VirtualInputManager.Instance.MoveLeft = true;
        }
        else
        {
            VirtualInputManager.Instance.MoveLeft = false;
        }
        if (Input.GetKey(KeyCode.Space))
        {
            VirtualInputManager.Instance.Jump = true;
        }
        else
        {
            VirtualInputManager.Instance.Jump = false;
        }
        if (Input.GetMouseButton(0))
        {
            VirtualInputManager.Instance.Attack = true;
        }
        else
        {
            VirtualInputManager.Instance.Attack = false;
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            VirtualInputManager.Instance.Roll = true;
        }
        else
        {
            VirtualInputManager.Instance.Roll = false;
        }

    }
}
