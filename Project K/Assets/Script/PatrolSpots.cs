using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PatrolSpots", menuName = "ScriptableObjects / PatrolSpots", order = 1)]
public class PatrolSpots : ScriptableObject
{
    public Transform[] patrolPoints;
    
}
