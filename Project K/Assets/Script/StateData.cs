using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract  class StateData : ScriptableObject
{
    public abstract void OnEnter(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo);
    public abstract void UpdateAbility(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo);
    public abstract void OnExit(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo);
}
