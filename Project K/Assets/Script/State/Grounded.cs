using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New State", menuName = "AbilityData/Gorunded")]
public class Grounded : StateData
{
    public float distance;
    public override void OnEnter(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {

    }
    public override void UpdateAbility(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {
        PlayerController control = charactorStateBase.GetPlayerController(animator);
        //Debug.Log("" + IsGrounded(control));
        if (IsGrounded(control,animator))
        {
            animator.SetBool("Grounded", true);
        }
        else
        {
            //Debug.Log("" + IsGrounded(control));
            animator.SetBool("Grounded", false);
        }


    }
    public override void OnExit(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {

    }
    bool IsGrounded(PlayerController control, Animator animator)
    {
        
        if (control.rigid.velocity.y > -0.01 && control.rigid.velocity.y <= 0)
        {
            //Debug.Log("" + control.rigid.velocity.y);
            return true;
        }

        if (control.rigid.velocity.y < 0f)
        {
            foreach (GameObject o in control.BottomSphere)
            {
                Debug.DrawRay(o.transform.position, -Vector3.up * 0.7f, Color.red);
                RaycastHit hit;
                if (Physics.Raycast(o.transform.position, -Vector3.up, out hit, distance))
                {
                    return true;
                }
            }
        }



        return false;
    }
}
