using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New State",menuName ="AbilityData/MoveForward")]
public class MoveForword : StateData
{
    public float Speed;
    public AnimationCurve speedGraph;
    public float blockDistance;
    public bool check;
    public override void OnEnter(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {
        check = false;
    }
    public override void UpdateAbility(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {
        PlayerController control = charactorStateBase.GetPlayerController(animator);

       
        if (VirtualInputManager.Instance.Jump)
        {
            animator.SetBool("Jump", true);
            
        }
        if (VirtualInputManager.Instance.MoveRight && VirtualInputManager.Instance.MoveLeft)
        {
            animator.SetBool("Move", false);
            
            return;
        }
        if (!VirtualInputManager.Instance.MoveRight && !VirtualInputManager.Instance.MoveLeft)
        {
            animator.SetBool("Move", false);

            return;
        }
        if (VirtualInputManager.Instance.Attack&& !animator.GetBool("Jump"))
        {
            animator.SetTrigger("Attack");
            animator.SetBool("Attacking", true);
        }
        if (VirtualInputManager.Instance.Roll)
        {
            animator.SetTrigger("Roll");
            animator.SetBool("Rolling", true);
        }

        if (VirtualInputManager.Instance.MoveRight)
        {
            //if(animator.GetBool("Move") == false)return;

            if (!CheckFront(control))
            {
                
                control.transform.Translate(Vector3.forward * Speed  * Time.deltaTime);
                control.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
                animator.SetBool("Move", true);

            }
            
        }
        else
        {
            //Debug.Log("" + check);
            if (VirtualInputManager.Instance.MoveLeft&& check && control.transform.rotation == Quaternion.Euler(0f, 90f, 0f))
            {
                //if (animator.GetBool("Move") == false) return;

                //if (!CheckFront(control))
                {

                    control.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                    control.transform.rotation = Quaternion.Euler(0f, 270f, 0f);
                    animator.SetBool("Move", true);
                    check = false;
                }

            }
        }
        
        if (VirtualInputManager.Instance.MoveLeft)
        {
           // if (animator.GetBool("Move") == false) return;
            
            if (!CheckFront(control))
            {
                
                control.transform.Translate(Vector3.forward * Speed  * Time.deltaTime);
                control.transform.rotation = Quaternion.Euler(0f, 270f, 0f);
                animator.SetBool("Move", true);
            }
                
        }
        else
        {
            if (VirtualInputManager.Instance.MoveRight && check && control.transform.rotation == Quaternion.Euler(0f, 270f, 0f))
            {
                //if (animator.GetBool("Move") == false) return;

                //if (!CheckFront(control))
                {

                    control.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                    control.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
                    animator.SetBool("Move", true);
                    check = false;

                }

            }
        }
    }

    public override void OnExit(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {
        
    }
    bool CheckFront(PlayerController control)
    {
        
        foreach (GameObject o in control.FrontSphere)
        {
            Debug.DrawRay(o.transform.position, control.transform.forward * 0.3f, Color.red);
            RaycastHit hit;
            if (Physics.Raycast(o.transform.position, control.transform.forward, out hit, blockDistance))
            {
                check = true;
                return true;
                
            }
        }
        
        return false;
    }
}
