using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New State", menuName = "AbilityData/Jump")]
public class Jump : StateData
{
    public float jumpForce;
    public AnimationCurve gravity;
    public AnimationCurve pull;
    public override void OnEnter(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {
        charactorStateBase.GetPlayerController(animator).rigid.AddForce(Vector3.up * jumpForce);
        animator.SetBool("Grounded", false);
    }
    public override void UpdateAbility(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {
        PlayerController control = charactorStateBase.GetPlayerController(animator);

        control.gravityMultiplier= gravity.Evaluate(stateInfo.normalizedTime);
        control.pullMultiplier= pull.Evaluate(stateInfo.normalizedTime);
    }
    public override void OnExit(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {

    }
}
