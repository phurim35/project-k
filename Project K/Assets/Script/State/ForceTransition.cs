using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New State", menuName = "AbilityData/ForceTransition")]
public class ForceTransition : StateData
{
    [Range(0.01f,1f)]
    public float TransitionTiming;

    public override void OnEnter(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {

    }
    public override void UpdateAbility(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {
        if (stateInfo.normalizedTime >= TransitionTiming)
        {
            animator.SetBool("ForceTransition", true);
            
        }
    }

    public override void OnExit(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {

        
    }
   
}
