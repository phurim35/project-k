using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New State", menuName = "AbilityData/Idle")]
public class Idle : StateData
{
    public override void OnEnter(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {
        animator.SetBool("Grounded", true);
    }
    public override void UpdateAbility(CharactorStateBase charactorStateBase, Animator animator,AnimatorStateInfo stateInfo)
    {
        
        if (VirtualInputManager.Instance.MoveRight && VirtualInputManager.Instance.MoveLeft)
        {
            animator.SetBool("Move", false);
            return;
        }
        if (VirtualInputManager.Instance.Attack)
        {
            animator.SetTrigger("Attack");
            animator.SetBool("Attacking", true);
        }
        if (!VirtualInputManager.Instance.MoveRight && !VirtualInputManager.Instance.MoveLeft&&!VirtualInputManager.Instance.Jump)
        {
            animator.SetBool("Move", false);

            return;
        }
        if (VirtualInputManager.Instance.Jump)
        {
            animator.SetBool("Jump", true);
        }
        if (VirtualInputManager.Instance.MoveRight)
        {

            animator.SetBool("Move", true);
        }
        if (VirtualInputManager.Instance.MoveLeft)
        {

            animator.SetBool("Move", true);
        }
        
    }
    public override void OnExit(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {
        
    }
}
