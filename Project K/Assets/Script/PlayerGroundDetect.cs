using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGroundDetect : MonoBehaviour
{
    public bool HightGround;
    public bool LowGround;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("LowGround"))
        {
            HightGround = false;
            LowGround = true;
            //Debug.Log("Low");
        }

        if (other.CompareTag("HighGround"))
        {
            HightGround = true;
            LowGround = false;
            //Debug.Log("High");
        }
    }
}
