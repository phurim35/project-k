using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassicPatrol : MonoBehaviour
{
    public float speed;
    public float distance;

    public bool movingRight;
    public bool movingLeft;
    public bool hitRight;
    

    public Transform groundDetection;

    public bool faceTarket;

    private void Update()
    {
        if (faceTarket) return;
        groundDetection.transform.parent = this.transform;
       
        Debug.DrawRay(groundDetection.position, Vector2.down * 0.3f, Color.red);
        RaycastHit hit;
        if (Physics.Raycast(groundDetection.position, Vector2.down, out hit, distance))
        {
           if(!movingLeft)
            {
                hitRight = true;
                
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
                if (movingRight == true)
                {
                    transform.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
                    
                }
            }
            if (movingLeft)
            {
                hitRight = false;
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
                
            }


        }
        else
        {
            if (movingRight)
            {
                movingRight = false;
                movingLeft = true;
                hitRight = true;
                //Debug.Log("Ground");
                if (!movingRight && movingLeft&&hitRight)
                {
                    
                    if (movingLeft == true)
                    {
                        transform.eulerAngles = new Vector3(0, 270f, 0);

                    }
                }
            }
            if (!movingRight)
            {

                if (!movingRight && movingLeft&&!hitRight)
                {
                    movingRight = true;
                    movingLeft = false;
                    transform.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
                    
                }
            }
            
            
            
           
            


        }
        
    }
}
