using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlimeController : MonoBehaviour
{
    public Animator animator;
    public float lookRadius;
    public float attackRadius;
    Transform target;
    public ClassicPatrol slime;
    public float speed;
    public PlayerGroundDetect playerGroundDetect;
    private Rigidbody rb;
    //public NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInParent(typeof(Animator)) as Animator;
        target = PlayerManager.Instance.player.transform;
        animator.SetBool("Walk", true);
        slime= GetComponentInParent(typeof(ClassicPatrol)) as ClassicPatrol;
        playerGroundDetect = GameObject.FindGameObjectWithTag("PlayerGroundDetect").GetComponent<PlayerGroundDetect>();
        rb = GetComponent<Rigidbody>();
        //agent=GetComponentInParent(typeof(NavMeshAgent)) as NavMeshAgent;
    }

    // Update is called once per frame
    void Update()
    {
        
        
            float distance = Vector3.Distance(target.position, transform.position);


        if (distance <= lookRadius)
        {
            if (playerGroundDetect.HightGround)
            {
                animator.SetBool("IsFollow", true);
                if (animator.GetBool("IsFollow") == true)
                {
                    slime.speed = 0;
                    speed = 1;

                    FaceDirection();
                    if (playerGroundDetect.HightGround)
                    {
                        slime.faceTarket = true;
                        if(animator.GetBool("Attack")==false)
                        rb.transform.position = Vector3.MoveTowards(rb.transform.position, target.position, speed * Time.deltaTime);
                    }
                    
                }




                if (distance <= attackRadius)
                {
                    //animator.SetBool("IsFollow", false);
                    speed = 0;
                    slime.speed = 0;
                    animator.SetBool("Attack", true);
                    

                }
                else
                {
                    speed = 1;
                    slime.speed = 1;
                    animator.SetBool("Attack", false);
                    
                }
            }
            if (playerGroundDetect.LowGround)
            {
                slime.faceTarket = false;
            }


        }
        else
        {
                speed = 1;
                slime.speed = 1;
                animator.SetBool("IsFollow", false);
                slime.faceTarket = false;
                //agent.SetDestination(this.agent.transform.position);
        }
        
        
    }
    void FaceDirection()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, attackRadius);
    }
}
