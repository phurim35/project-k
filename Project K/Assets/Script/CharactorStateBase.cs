using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactorStateBase : StateMachineBehaviour
{
    public List<StateData> ListAbilityData = new List<StateData>();

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach(StateData d in ListAbilityData)
        {
            d.OnEnter(this, animator, stateInfo);
        }
    }

    public void UpdateAll(CharactorStateBase charactorStateBase, Animator animator, AnimatorStateInfo stateInfo)
    {
        foreach(StateData d in ListAbilityData)
        {
            d.UpdateAbility(charactorStateBase, animator,stateInfo);
        }
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        UpdateAll(this, animator, stateInfo);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach (StateData d in ListAbilityData)
        {
            d.OnEnter(this, animator, stateInfo);
        }
    }
    private PlayerController playerController;
    public PlayerController GetPlayerController(Animator animator)
    {
        if (playerController == null)
        {
            playerController = animator.GetComponentInParent<PlayerController>();
        }
        return playerController;
    }
}
