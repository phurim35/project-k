using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Stagger : MonoBehaviour
{
    public Text text;
    public Slider bar;
    public int maximumgage;
    private int currentgage;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void Update()
    {
        //for test
        Testlogic();


        UpdateBar();

        if (currentgage >= maximumgage)
        {
            //chang animation status

            //countdown to recovery

            text.text = "status : stagger";
        }
        else
        {
            text.text = "status : ";
        }
        
    }
    private void UpdateBar()
    {
        bar.value = currentgage;
    }
   
    public void getStagger(int stack)
    {
        currentgage += stack;
    }
    private void Testlogic()
    {
        if (Input.GetKeyDown(KeyCode.Q)) {
            getStagger(10);
        }
    }

    
   
}
