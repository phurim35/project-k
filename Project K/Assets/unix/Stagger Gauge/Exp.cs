using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exp : MonoBehaviour
{
       [SerializeField]
    private int LVnumber=1;
    public int levelcap;
    public int currentExp;

    private int skillpoint = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        levelUp();
    }
    public void getExp(int exp)
    {
        currentExp += exp;
    }
    private void levelUp()
    {
        if (currentExp >= levelcap)
        {
            var tmp = currentExp - levelcap;

            currentExp = 0;
            LVnumber++;
            //get skill pont
            skillpoint++;
            currentExp += tmp;


        }
    }
}
