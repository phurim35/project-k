using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class detectplayer : MonoBehaviour
{
    public GameObject text;
    public bool trigger = false;
    public GameObject UISkill;
    public GameObject UILevelup;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (trigger)
        {
            text.SetActive(true);
            UISkill.SetActive(true);
            UILevelup.SetActive(true);
        }
       
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other == gameObject.CompareTag("Player"))
        {
            trigger = true;
        }
        else { trigger = false; }
    }
}
