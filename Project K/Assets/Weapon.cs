using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : StateMachineBehaviour
{
    [Range(0.01f, 1f)]
    public float FireTiming;
    public Rigidbody arrow;
    public Transform bow;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        bow= GameObject.FindGameObjectWithTag("Bow").GetComponent<Transform>();

        FireTiming = 0.64f;

        Rigidbody arrowInstance;
        arrowInstance = Instantiate(arrow, bow.position, bow.rotation) as Rigidbody;
        arrowInstance.AddForce(bow.forward * 600f);
        
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        FireTiming -= Time.deltaTime ;
        
        if (FireTiming <= 0)
        {
            FireTiming = 0.64f;

            Rigidbody arrowInstance;
            arrowInstance = Instantiate(arrow, bow.position,bow.rotation) as Rigidbody;
            arrowInstance.AddForce(bow.forward * 600f);
            
            





        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
