using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets;

public class Trap : MonoBehaviour
{
    //private float StunTime;
    //public float CoolDownStun;
    private bool Stun;
    //private GameObject _player;
    private PlayerController _playercontroller;
    //private float _stuntime;
    

    void Update()
    {
        _playercontroller = FindObjectOfType<PlayerController>();
        Stun = _playercontroller.gameObject.GetComponent<PlayerController>().IsStun;
        //_stuntime = _playercontroller.gameObject.GetComponent<PlayerController>().CoolDownStun;
        //if (Stun)
        //{
        //    CoolDown();
        //}
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            //other.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>().m_MoveSpeedMultiplier = 0;
            //_player = other.gameObject;
            Stun = true;
            //_stuntime = CoolDownStun;
            _playercontroller.gameObject.GetComponent<PlayerController>().IsStun = Stun;
            //_playercontroller.gameObject.GetComponent<PlayerController>().CoolDownStun = _stuntime;
            Destroy(this.gameObject);
        }
    }

    //public void CoolDown()
    //{
    //    //_player.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().enabled = false;
    //    Debug.Log("Hit");
    //    if (Time.time - StunTime < CoolDownStun)
    //    {
    //        return;
    //    }
    //    StunTime = Time.time;
    //    Stun = false;
    //    _playercontroller.gameObject.GetComponent<PlayerController>().IsStun = Stun;
    //    Debug.Log("Done");
    //    //_player.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().enabled = true;
    //    //_player.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>().m_MoveSpeedMultiplier = 1;
    //    Destroy(this.gameObject);
    //}
}
