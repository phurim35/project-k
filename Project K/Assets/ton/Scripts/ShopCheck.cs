using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopCheck : MonoBehaviour
{
    public GameObject ShopUI;
    public GameObject PopupShopCanvas;
    public bool shopIsOpen;
    private bool IsOpen;

    public Dialogue dialogue;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && !IsOpen)
        {
            if (shopIsOpen)
            {
                ShopOpen();
            }
            else
                Debug.Log("Go to Shop");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            shopIsOpen = true;
            PopupShopCanvas.SetActive(true);
            Debug.Log("Ready to enter shop");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PopupShopCanvas.SetActive(false);
            ShopClose();
        }
    }

    public void ShopOpen()
    {
        IsOpen = true;
        TriggerDialogue();
        //ShopUI.SetActive(true);
    }

    public void ShopClose()
    {
        IsOpen = false;
        shopIsOpen = false;
        ShopUI.SetActive(false);
        DisableDialogue();
        Debug.Log("Shop Close");
    }

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }

    public void DisableDialogue()
    {
        FindObjectOfType<DialogueManager>().EndDialogue();
    }
}
